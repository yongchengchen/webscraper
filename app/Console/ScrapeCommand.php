<?php
/**
 * @copyright
 * @license
 * @author      Yongcheng Chen yongcheng.chen@live.com
 */
namespace App\Console;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\DomCrawler\Crawler;


use App\Model\BrowserSimulator;

class ScrapeCommand extends Command
{
    protected function configure()
    {
  	$this->setName('app:scrape')
             ->setDescription('Scrape a web page.')
	     ->addArgument('url', InputArgument::REQUIRED,
		'The url');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $url = $input->getArgument('url');
	if (!$url || (strpos($url, 'http:') !== 0 && strpos($url, 'https:') !== 0)) {
		#$output->writeln('Please input http url');
		#return false;
	}
	
	$simulator = new BrowserSimulator();
	$response = $simulator->loadCookieFromJsonFile(__DIR__ . '/../cookies/cookie.json')
            ->setDelay(3)
            ->addSetting('userAgent', 'Mozilla/5.0 (iPhone; CPU iPhone OS 6_0 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Version/6.0 Mobile/10A5376e Safari/8536.25')
            // ->addSetting('userAgent', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:33.0) Gecko/20120101 Firefox/33.0')
            ->request('https://shop.m.taobao.com/shop/coupon.htm?sellerId=2108101630&activityId=cf00f3f21eef4175afa365c0b1629a5c');

	if ($response->getStatus() < 400) {
		$crawler = new Crawler($response->getContent());
		$divs = $crawler->filterXPath('descendant-or-self::body/div');
		foreach ($divs as $domElement) {
		    var_dump($domElement);
		}
	}
	#$output->writeln('status:' . $response->getContent() );
	$output->writeln('status:' . $response->getStatus() );
    }
}
