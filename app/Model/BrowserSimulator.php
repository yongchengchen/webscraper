<?php
/**
 * @copyright
 * @license
 * @author      Yongcheng Chen yongcheng.chen@live.com
 */

namespace App\Model;

// use Symfony\Component\BrowserKit\Client as BaseClient;
use Symfony\Component\BrowserKit\Request;
use Symfony\Component\BrowserKit\Response;
use JonnyW\PhantomJs\DependencyInjection\ServiceContainer;
use JonnyW\PhantomJs\Client as Simulator;

class BrowserSimulator
{
    const TMP_DIR = '/tmp/webcraper/';
    const CACHE_DIR = '/tmp/webcraper/cache';

    const REQUEST_CALLBAK_METHODS = [
        'addSetting',
        'addCookie',
        'addHeader',
        'setDelay',
        'setTimeout',
        'setViewportSize',
        'setRequestData',
        'setHeaders',
        'addHeader',
        'addHeaders',
        'addCookie',
        'deleteCookie',
        'setBodyStyles',
    ];

    protected $_procedureLoaded = false;
    protected $_requestParamsQueue = [];

    
    //only support functions below
    //setTimeout($timeout)
    //setDelay($delay)
    //setViewportSize($width, $height)
    // setRequestData(array $data)
    //setHeaders(array $headers)
    //addHeader($header, $value)
    // addHeaders(array $headers)
    //addSetting($setting, $value)
    //addCookie($name, $value, $path, $domain, $httpOnly = true, $secure = false, $expires = null)
    //deleteCookie($name)
    //setBodyStyles(array $styles)
    //
    public function __call($method, $argv) {
        if (in_array($method, self::REQUEST_CALLBAK_METHODS)) {
            $this->pushToRequestParamsQueue($method, $argv);
            return $this;
        }
        return false;
    }

    public function loadCookieFromJsonFile($filename) {
        $cookies = json_decode(file_get_contents($filename));
        foreach($cookies as $cookie) {
            $this->addCookie($cookie->name, $cookie->value, $cookie->path, $cookie->domain,
                $cookie->hostOnly, $cookie->secure, 2201558220068);
        }
        return $this;
    }
    
    protected function pushToRequestParamsQueue($funcName, $argv) {
        $this->_requestParamsQueue[] = [$funcName, $argv];
    }

    public function addExtraProcedure($task_name = null) {
        if (!$this->_procedureLoaded) {
            $common = __DIR__ .'/procedures/common';
            $dir = $common;
            if (!empty($task_name)) {
                $taksPath = __DIR__ .'/procedures/' . $task_name;
                $dir = sprintf('%s/%s/', self::TMP_DIR, $pid);
                exec('mkdir -p '. $dir);
                exec(sprintf('cp -a %s/* %s', $common, $dir));
                exec(sprintf('cp -a %s/* %s', $taksPath, $dir));
            }

            $procedureLoader = ServiceContainer::getInstance()->get('procedure_loader_factory')->createProcedureLoader($dir);
            Simulator::getInstance()->getProcedureLoader()->addLoader($procedureLoader);
        }
        $this->_procedureLoaded = true;
        return $this;
    }

    protected function prepareDefault() {
        $this->addExtraProcedure();
        Simulator::getInstance()->getEngine()->setPath(base_path('bin/phantomjs'));
        Simulator::getInstance()->getEngine()->addOption('--load-images=false');
        Simulator::getInstance()->getEngine()->addOption('--disk-cache-path=' . self::CACHE_DIR);
        return $this;
    }

    protected function prepareRequest($request) {
        foreach($this->_requestParamsQueue as $clouser) {
            call_user_func_array([$request, $clouser[0]], $clouser[1]);
        }
        return $request;
    }

    /**
     * @param Request $request
     *
     * @return Response
     */
    Public function request($url, $method = 'GET')
    {
        $msgFactory = Simulator::getInstance()->getMessageFactory();
        $request  = $this->prepareRequest($msgFactory->createRequest($url, $method, '100000'));
        $response = $msgFactory->createResponse();
        Simulator::getInstance()->send($request, $response);
        return new Response((string) $response->getContent(), $response->getStatus(), $response->getHeaders());
    }

}
