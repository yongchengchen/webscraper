<?php
/**
 * @copyright
 * @license
 * @author      Yongcheng Chen yongcheng.chen@live.com
 */

require __DIR__.'/vendor/autoload.php';

#print_r(\JonnyW\PhantomJs\Client::getInstance());

use Symfony\Component\Console\Application;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use App\Console\ScrapeCommand;

$application = new Application('Scraper', '1.0.0');
$command = new ScrapeCommand();

$application->add($command);

#$application->setDefaultCommand($command->getName(), true);
$application->run();
